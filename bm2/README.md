bm2: Hitachi MB-6885 Basic Master Jr. Emulator
==============================================

This is a copy of the [日立ベーシックマスターJr.エミュレータ bm2][hp]
emulator. The files in this directory are:

- `README.md`: This file, not part of the original distribution.
- [`Archive/`]: copies of the original distribution archives.
- `bm2/`: An extracted copy of the `bm2src_20161113.tgz` archive, created
  by the top-level [`Makefile`].
- `gtr-readme`: A copy of `bm2/readme.txt` passed through Google Translate.
  This may be a helpful gloss to those with limited Japanese skills who are
  trying to read the orginal document.

The archives under [`Archive/`] are downloaded from the home page:
- `bm2src_20161113.tgz`: Source code for Unix/Linux.

### Building

The sole major dependency for the Linux version is SDL2. (Possibly it can
be built with SDL1.2 as well, but that's not been tested. You can confirm
that you have it by running `sdl2-config --version`; if you don't you can
`apt-get install libsdl2-dev`. Typing `make` in that directory should
produce a `bm2` executable.


bm2 Usage
---------

The emulator itself includes no program ROM, just the default font ROM (not
accessible to the user). When started with a non-blank `rom_dir`
configuration option the "emulated ROM" (see below) is used. This includes
a simple monitor (`bm2sub.c:monitor()`) described below.

### Key Bindings

The key bindings are based on scan codes; the table below gives the MB-6885
key(s) followed by the standard US keyboard binding for that scan code.

    MB-688x             US
    ────────────────────────────────
    Break               Esc
    英数                L-Ctrl
    英記号              L-Shift
    カナ                R-Shift
    カナ記号            R-Ctrl
    @ ﾎ ﾍ               \ - =
    start-of-tape       PgUp
    end-of-tape         PgDn
    select tape img     Alt-F
    paste clipboard     Alt-P

Keys can be remapped; this is not yet documented here.

### Initialisation

`readme.txt` mentions "Drop the file onto the bm2 icon"; in Windows this
sets the first argument to the filename of the dropped file.

Options may be given in `NAME VALUE` format in the file `bm2config` in the
current working directory or `-NAME=VALUE` format on the command line,
overriding any `bm2config` options (see `init.c:init(),
conf.c:getConfig()`).

The command line may also include a filename (not starting with `-`). If
it's a file containing S records these will be loaded (`S9` start address
records are ignored), otherwise it will be attached as a tape image file.
(Non-existent files will be created when the CMT is first written.)

CMT files are binary, not sound files; the standard extension is `.bin` and
the format is apparently that of `bintape`. `SAVE` and examine a file to
see the format.

If using the Emulated ROM monitor (i.e., `rom_dir` is unset or blank: force
with `-rom_dir=` on the command line), a second argument in `scanf("%x")`
format may be given after the S-record filename. This gives an address to
be deposited to the reset vector ($FFFE); the default is $C000. The default
address for the emulated monitor's `G` command (followed by a space) will
be the lowest address read the S-record file.

#### Options

`rom_dir` gives a directory from which it tries to read S-record files
first, then binary files. Both S-record files or all three `.rom` files
must exist and have a length greater than 0. Short files will be filled
with zeros out to the end of that ROM block; over-length files will be
truncated.

      S-record        Binary
      ───────────────────────────────────────────────────────────────────
      b000-e7ff.s     bas.rom ($B000 len $3000), prt.rom ($E000 len $800)
      f000-ffff.s     mon.rom ($F000 len $1000)

- `zoom n` where _n_ is 1-4: screen size (default `2`)
- `full_line y`: does not print every other line to emulate 200p. (default `n`)
- `ram_size 16k | 64k`: (default 16k)

See `readme.txt` for more options.

### Emulated ROM Monitor

The emulated $F000 `monitor` routine prompts `* INPUT COMMAND   ` and
accepts the following commands:

- `L`,`l`: Prompts `PROGRAM NAME : ` and loads S-records from the given
  filename. Prints `OK` on successful load or `NG` on error. (XXX need to
  figure out what's up with the extensions, etc.)
- `G`,`g`: Prompts `START ADDRESS: `, reads up to 4 hex digits,
  terminated by a space, and jumps to that address. CR aborts input.


bm2 Design and Theory of Operation
----------------------------------

### ROM Routine Emulation

The emulator itself includes no ROM. It does, however, have _emulation_ (in
C code) of some ROM routines. The Makefile by default defines `M68_SUB`
which compiles in the ROM routine emulation. It is actually used at runtime
only if `m68->emulate_subroutine` is set to TRUE, which is done by
`init.c:init()` when the `rom_dir` configuration option has a blank value.
That also causes some memory locations to be initialised:

    0028: 7E F0 12              # ASCIN:  JMP $F012 (CHRGET)
    002B: 7E F0 15              # ASCOUT: JMP $F015 (CHROUT)
    C000: BD F0 00 7E C0 00     # BASIC:  JSR $F000 (monitor), JMP $C000
    FFF8: F0 4D                 # IRQ
    FFFE: C0 00                 # RESET (or start addr from command line)

The emulated ROM routines are in `bm2sub.c`. When enabled,
`m68subroutine()` is called in the following situations. If it returns >0
it's determined to have handled the situation and some unwinding may occur,
otherwise execution continues as it normally would.

- A `JMP` or `JSR` instruction is executed; the post-branch PC is passed
  in. If handled, the next PC will be pulled from the stack and the return
  value will be added to `_states`.
- An NMI or IRQ is generated. The standard stack frame is built and the
  vector from $FFFC or $FFF8 is passed in. If handled, the stack frame is
  unwound as RTI would do.
- The system is reset (startup or かな記号-Break); the vector from $FFFE
  will be passed in. The routine may update the PC and system state as
  necessary or do nothing. Return value is ignored. (With the default $C000
  reset vector there's no match and code at $C000 starts executing.)

`m68subroutine()` provides the following emulations (functions in `bm2sub.c`):

    $E14F: clrtxt
    $E38D: clrgrp1
    $E39C: clrgrp2
    $F000: monitor
    $F003: addixb
    $F009: movblk
    $F00C: music
    $F00F: kbin
    $F012: chrget
    $F015: chrout
    $F018: load
    $F01B: save
    $F04D: timer
    $FFE6: nmiset
    $FFE9: nmiclr
    $FFEC: outix
    $FFEF: clrtv
    $FFF2: mesout
    $FFF5: curpos

See above for the user interface for the $F000 `monitor` routine.

### Memory and I/O

In `b2mem.c`, the `m68read8()` `m68write8()` give the memory map.



<!-------------------------------------------------------------------->
[`Archive/`]: ./Archive/
[`Makefile`]: ../Makefile
[hp]: http://ver0.sakura.ne.jp/pc/#bm2
